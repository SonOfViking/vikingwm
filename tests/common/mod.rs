use slog::{o, Drain};
use smithay::reexports::{calloop::EventLoop, wayland_server::Display};
use std::{cell::RefCell, rc::Rc};
use vikingwm::{
    configuration::{configuration::get_config, self},
    workspace::workspace::Workspace,
};

pub struct TestWM
{
    pub workspaces: Vec<Workspace>,
    pub current_ws: usize,
}

pub fn setup() -> TestWM
{
    use core::panic;

    let log = if std::env::var("ANVIL_MUTEX_LOG").is_ok()
    {
        slog::Logger::root(
            std::sync::Mutex::new(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    }
    else
    {
        slog::Logger::root(
            slog_async::Async::default(slog_term::term_full().fuse()).fuse(),
            o!(),
        )
    };
    let config = get_config(Some(&log));
    TestWM::init(&config)

}
impl TestWM
{
    pub fn init(cfg:&configuration::configuration::Config) -> TestWM
    {
        let mut workspaces = Vec::new();
        for ws in &cfg.screen.workspaces
        {
            workspaces.push(Workspace::new(ws.to_owned()));
        }
        TestWM { workspaces, current_ws: 0, }
    }

    pub fn current_ws(&mut self) -> &mut Workspace
    {
        &mut self.workspaces[self.current_ws]
    }
}
