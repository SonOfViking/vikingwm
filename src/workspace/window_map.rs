use std::cell::RefCell;
use std::sync::Mutex;

use smithay::{
    reexports::{
        wayland_protocols::xdg_shell::server::xdg_toplevel,
        wayland_server::protocol::wl_surface::{self, WlSurface},
    },
    utils::{Logical, Point, Rectangle, Size},
    wayland::{
        compositor::{
            with_states, with_surface_tree_downward, SubsurfaceCachedState, TraversalAction,
        },
        shell::{
            legacy::ShellSurface,
            wlr_layer::Layer,
            xdg::{
                PopupSurface, SurfaceCachedState, ToplevelSurface, XdgPopupSurfaceRoleAttributes,
            },
        },
    },
};

use crate::backend::shell::SurfaceData;
#[cfg(feature = "xwayland")]
use crate::xwayland::X11Surface;

pub use crate::workspace::layer_map::{LayerMap, LayerSurface};

#[derive(Debug, Clone, PartialEq)]
pub enum Kind
{
    Xdg(ToplevelSurface),
    Wl(ShellSurface),
    #[cfg(feature = "xwayland")]
    X11(X11Surface),
    Empty,
}

impl Kind
{
    pub fn alive(&self) -> bool
    {
        match *self
        {
            Kind::Xdg(ref t) => t.alive(),
            Kind::Wl(ref t) => t.alive(),
            #[cfg(feature = "xwayland")]
            Kind::X11(ref t) => t.alive(),
            Kind::Empty => false,
        }
    }

    pub fn get_surface(&self) -> Option<&wl_surface::WlSurface>
    {
        match *self
        {
            Kind::Xdg(ref t) => t.get_surface(),
            Kind::Wl(ref t) => t.get_surface(),
            #[cfg(feature = "xwayland")]
            Kind::X11(ref t) => t.get_surface(),
            Kind::Empty => None,
        }
    }

    /// Activate/Deactivate this window
    pub fn set_activated(&self, active: bool)
    {
        if let Kind::Xdg(ref t) = self
        {
            let changed = t.with_pending_state(|state| {
                if active
                {
                    state.states.set(xdg_toplevel::State::Activated)
                }
                else
                {
                    state.states.unset(xdg_toplevel::State::Activated)
                }
            });
            if let Ok(true) = changed
            {
                t.send_configure();
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum PopupKind
{
    Xdg(PopupSurface),
}

impl PopupKind
{
    pub fn alive(&self) -> bool
    {
        match *self
        {
            PopupKind::Xdg(ref t) => t.alive(),
        }
    }

    pub fn get_surface(&self) -> Option<&wl_surface::WlSurface>
    {
        match *self
        {
            PopupKind::Xdg(ref t) => t.get_surface(),
        }
    }

    pub fn parent(&self) -> Option<wl_surface::WlSurface>
    {
        let wl_surface = match self.get_surface()
        {
            Some(s) => s,
            None => return None,
        };
        with_states(wl_surface, |states| {
            states
                .data_map
                .get::<Mutex<XdgPopupSurfaceRoleAttributes>>()
                .unwrap()
                .lock()
                .unwrap()
                .parent
                .clone()
        })
        .ok()
        .flatten()
    }

    pub fn location(&self) -> Point<i32, Logical>
    {
        let wl_surface = match self.get_surface()
        {
            Some(s) => s,
            None => return (0, 0).into(),
        };
        with_states(wl_surface, |states| {
            states
                .data_map
                .get::<Mutex<XdgPopupSurfaceRoleAttributes>>()
                .unwrap()
                .lock()
                .unwrap()
                .current
                .geometry
        })
        .unwrap_or_default()
        .loc
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Window
{
    // TODO: Add a name or something?
    pub location: Point<i32, Logical>,
    /// A bounding box over this window and its children.
    ///
    /// Used for the fast path of the check in `matching`, and as the fall-back for the window
    /// geometry if that's not set explicitly.
    pub bbox: Rectangle<i32, Logical>,
    pub toplevel: Kind,
}

impl std::fmt::Display for Window
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        let size = self.bbox.size;
        write!(f, "location: {:?}, size: {:?}", self.location, size)
    }
}

impl Window
{
    pub fn create(location: Point<i32, Logical>, surface: &mut Option<Kind>) -> Window
    {
        let toplevel = surface.get_or_insert(Kind::Empty).to_owned();
        Window {
            location,
            bbox: Rectangle::default(),
            toplevel,
        }
    }
    pub fn surface(&self) -> &Kind
    {
        &self.toplevel
    }
    pub fn set_location(&mut self, new_locc: &(i32, i32))
    {
        self.location.x = new_locc.0;
        self.location.y = new_locc.1;
    }
    pub fn get_location(&self) -> Point<i32, Logical>
    {
        self.location
    }
    pub fn set_geometry(&mut self, new_geom: Size<i32, Logical>)
    {
        // println!("send geometry = {:?}", new_geom);
        if let Kind::Xdg(surface) = &self.toplevel
        {
            let ret = surface.with_pending_state(|state| {
                // state.states.set(xdg_toplevel::State::TiledRight);
                state.size = Some(new_geom);
            });
            if ret.is_ok()
            {
                surface.send_configure();
            }
        }
        self.bbox = Rectangle::from_loc_and_size(self.location, new_geom.to_owned());
    }
    /// Finds the topmost surface under this point if any and returns it together with the location of this
    /// surface.
    pub fn matching(
        &self,
        point: Point<f64, Logical>,
    ) -> Option<(wl_surface::WlSurface, Point<i32, Logical>)>
    {
        if !self.bbox.to_f64().contains(point)
        {
            return None;
        }
        // need to check more carefully
        let found = RefCell::new(None);
        if let Some(wl_surface) = self.toplevel.get_surface()
        {
            with_surface_tree_downward(
                wl_surface,
                self.location,
                |wl_surface, states, location| {
                    let mut location = *location;
                    let data = states.data_map.get::<RefCell<SurfaceData>>();

                    if states.role == Some("subsurface")
                    {
                        let current = states.cached_state.current::<SubsurfaceCachedState>();
                        location += current.location;
                    }

                    let contains_the_point = data
                        .map(|data| {
                            data.borrow().contains_point(
                                &*states.cached_state.current(),
                                point - location.to_f64(),
                            )
                        })
                        .unwrap_or(false);
                    if contains_the_point
                    {
                        *found.borrow_mut() = Some((wl_surface.clone(), location));
                    }

                    TraversalAction::DoChildren(location)
                },
                |_, _, _| {},
                |_, _, _| {
                    // only continue if the point is not found
                    found.borrow().is_none()
                },
            );
        }
        found.into_inner()
    }

    pub fn self_update(&mut self)
    {
        let mut bounding_box = Rectangle::from_loc_and_size(self.location, (0, 0));
        if let Some(wl_surface) = self.toplevel.get_surface()
        {
            with_surface_tree_downward(
                wl_surface,
                self.location,
                |_, states, &loc| {
                    let mut loc = loc;
                    let data = states.data_map.get::<RefCell<SurfaceData>>();

                    if let Some(size) = data.and_then(|d| d.borrow().size())
                    {
                        if states.role == Some("subsurface")
                        {
                            let current = states.cached_state.current::<SubsurfaceCachedState>();
                            loc += current.location;
                        }

                        // Update the bounding box.
                        bounding_box = bounding_box.merge(Rectangle::from_loc_and_size(loc, size));

                        TraversalAction::DoChildren(loc)
                    }
                    else
                    {
                        // If the parent surface is unmapped, then the child surfaces are hidden as
                        // well, no need to consider them here.
                        TraversalAction::SkipChildren
                    }
                },
                |_, _, _| {},
                |_, _, _| true,
            );
        }
        self.bbox = bounding_box;
    }

    /// Returns the geometry of this window.
    pub fn geometry(&self) -> Rectangle<i32, Logical>
    {
        // No surface set so return bounding_box
        if self.toplevel.get_surface().is_none()
        {
            return self.bbox;
        }
        // It's the set geometry with the full bounding box as the fallback.
        with_states(self.toplevel.get_surface().unwrap(), |states| {
            states.cached_state.current::<SurfaceCachedState>().geometry
        })
        .unwrap()
        .unwrap_or(self.bbox)
    }

    /// Sends the frame callback to all the subsurfaces in this
    /// window that requested it
    pub fn send_frame(&self, time: u32)
    {
        if let Some(wl_surface) = self.toplevel.get_surface()
        {
            with_surface_tree_downward(
                wl_surface,
                (),
                |_, _, &()| TraversalAction::DoChildren(()),
                |_, states, &()| {
                    // the surface may not have any user_data if it is a subsurface and has not
                    // yet been commited
                    SurfaceData::send_frame(&mut *states.cached_state.current(), time)
                },
                |_, _, &()| true,
            );
        }
    }
}

#[derive(Debug, Clone)]
pub struct Popup
{
    pub popup: PopupKind,
}
