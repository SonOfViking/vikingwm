use crate::workspace::window_map::{Kind, LayerMap, Popup, PopupKind, Window};
use smithay::wayland::shell::xdg::ToplevelState;
use smithay::{
    reexports::{
        wayland_protocols::xdg_shell::server::xdg_toplevel,
        wayland_server::protocol::wl_surface::{self, WlSurface},
    },
    utils::{Logical, Point, Rectangle, Size},
    wayland::shell::wlr_layer::Layer,
};

#[derive(Debug, PartialEq, Clone)]
pub enum Layout {
    Tiling,     // Tile windows in the master-stack layout
    VertTiling, // Tile windows in the vertical master-stack layout, useful for vertical monitors
    Fullscreen, // Set windows to fullscreen
    Floating,   // Float windows
}

// this is the container for multiple Windows @see struct Window
// it takes up the entire Output, so it has the same size as the output
#[derive(Debug, Clone)]
pub struct Workspace {
    pub name: String,
    layout: Layout,
    windows: Vec<Window>,
    popups: Vec<Popup>,
    pub layers: LayerMap,
    pub current_window: usize,
    output_size: Size<i32, Logical>,
}

impl std::fmt::Display for Workspace {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "name:{}, layout: {:?}", &self.name, &self.layout)?;
        if self.windows.len() > 0 {
            write!(f, "windows:\n")?;
            for w in &self.windows {
                write!(f, "{}\n", w)?;
            }
            write!(f, "current_window: {}", &self.current_window)?;
        }
        Ok(())
    }
}

impl Workspace {
    pub fn new(name: String /*output_size:Size<i32, Logical>*/) -> Workspace {
        Workspace {
            name,
            layout: Layout::Tiling,
            windows: Vec::new(),
            popups: Vec::new(),
            layers: Default::default(),
            current_window: 0,
            output_size: (0, 0).into(),
        }
    }

    pub fn set_size(&mut self, size: Size<i32, Logical>) {
        // println!("setting size to {:?} for ws {}", size, self.name);
        self.output_size = size;
        self.arrange(None);
    }

    pub fn windows(&self) -> &Vec<Window> {
        &self.windows
    }

    // Swap removes the current window
    pub fn swap_remove(&mut self) -> Option<Window> {
        let window = self.current_window()?;
        let idx = self.windows.iter().position(|w| w == window)?;
        Some(self.windows.swap_remove(idx))
    }

    pub fn insert(&mut self, window: &mut Window) {
        println!("Pushing window in workspace {} of size {:?}", self.name, self.output_size);
        window.self_update();
        self.windows.push(window.to_owned());
        self.arrange(None);
        // self.current_window = self.windows.len() - 1;
    }

    pub fn layout(&mut self, l: Layout) {
        self.layout = l;
    }

    pub fn has_layout(&self, l: Layout) -> bool {
        l == self.layout
    }

    pub fn current_window(&self) -> Option<&Window> {
        if self.windows.len() == 0 {
            return None;
        }
        Some(&self.windows[self.current_window])
    }

    pub fn refresh(&mut self) {
        self.windows.retain(|w| w.toplevel.alive());
        self.popups.retain(|p| p.popup.alive());
        self.layers.refresh();
        // for w in &mut self.windows {
        //     w.self_update();
        // }
    }

    pub fn send_frames(&self, time: u32) {
        for window in &self.windows {
            window.send_frame(time);
        }
        self.layers.send_frames(time);
    }

    pub fn clear(&mut self) {
        self.windows.clear();
    }
    /// Returns the geometry of the toplevel, if it exists.
    pub fn geometry(&self, toplevel: &Kind) -> Option<Rectangle<i32, Logical>> {
        self.windows
            .iter()
            .find(|w| &w.toplevel == toplevel)
            .map(|w| w.geometry())
    }

    pub fn insert_popup(&mut self, popup: PopupKind) {
        let popup = Popup { popup };
        self.popups.push(popup);
    }

    /// Finds the popup corresponding to the given `WlSurface`.
    pub fn find_popup(&self, surface: &wl_surface::WlSurface) -> Option<PopupKind> {
        self.popups.iter().find_map(|p| {
            if p.popup
                .get_surface()
                .map(|s| s.as_ref().equals(surface.as_ref()))
                .unwrap_or(false)
            {
                Some(p.popup.clone())
            } else {
                None
            }
        })
    }
    /// Refreshes the state of the toplevel, if it exists.
    pub fn refresh_toplevel(&mut self, toplevel: &Kind) {
        if let Some(w) = self.windows.iter_mut().find(|w| &w.toplevel == toplevel) {
            w.self_update();
        }
    }
    /// Sets the location of the toplevel, if it exists.
    pub fn set_location(&mut self, toplevel: &Kind, location: Point<i32, Logical>) {
        if let Some(w) = self.windows.iter_mut().find(|w| &w.toplevel == toplevel) {
            w.location = location;
            w.self_update();
        }
    }
    /// Finds the toplevel corresponding to the given `WlSurface`.
    pub fn find(&self, surface: &wl_surface::WlSurface) -> Option<Kind> {
        self.windows.iter().find_map(|w| {
            if w.toplevel
                .get_surface()
                .map(|s| s.as_ref().equals(surface.as_ref()))
                .unwrap_or(false)
            {
                Some(w.toplevel.clone())
            } else {
                None
            }
        })
    }
    /// Returns the location of the toplevel, if it exists.
    pub fn location(&self, toplevel: &Kind) -> Option<Point<i32, Logical>> {
        self.windows
            .iter()
            .find(|w| &w.toplevel == toplevel)
            .map(|w| w.location)
    }

    pub fn close_current(&mut self) {
        if let Some(current_window) = self.current_window() {
            if let Kind::Xdg(sfc) = current_window.surface() {
                sfc.send_close();
            }
        }
        if self.current_window > 0 {
            self.current_window -= 1;
        } else {
            self.current_window = 0;
        }
        self.arrange(None);
    }

    pub fn arrange(&mut self, log: Option<&slog::Logger>) {
        match self.layout {
            Layout::Tiling => self.arrange_tiling(log),
            Layout::VertTiling => self.arrange_vert_tiling(log),
            Layout::Fullscreen => self.arrange_fullscreen(log),
            Layout::Floating => self.arrange_floating(log),
        }
    }

    fn arrange_fullscreen(&mut self, log: Option<&slog::Logger>) {
        if self.windows.len() == 0 {
            return; // No need to do anything
        }
        let _ = log;

        // xdgRequest Fullscreen for the currently focussed window, xdgRequest minimize for the
        // rest of them
        //
        // set all screens to minimize
        // set current_window to fullscreen

        // minimizing all windows
        // TODO: Actually minimize
        let size = (0, 0);
        for window in &mut self.windows.iter_mut() {
            window.set_location(&(0, 0));

            match window.surface() {
                Kind::Xdg(surface) => {
                    let ret = surface.with_pending_state(|state| {
                        let s = ToplevelState::default();
                        state.states = s.states;
                        state.size = Some(size.into());
                    });
                    if ret.is_ok() {
                        surface.send_configure();
                    }
                }
                Kind::Empty => {
                    window.set_geometry(size.into());
                }
                Kind::Wl(_) => {} // Should we do something here?
            }
        }
        let w = &mut self.windows[self.current_window];
        let size = self.output_size;
        match w.surface() {
            Kind::Xdg(surface) => {
                let ret = surface.with_pending_state(|state| {
                    let s = ToplevelState::default();
                    state.states = s.states;
                    state.states.set(xdg_toplevel::State::Fullscreen);
                    state.size = Some(size.into());
                });
                if ret.is_ok() {
                    surface.send_configure();
                }
            }
            Kind::Empty => {
                w.set_geometry(size);
            }
            Kind::Wl(_) => {} // Should we do something here?
        }
    }

    fn set_window_tiling(w: &mut Window, location: &(i32, i32), size: Size<i32, Logical>) {
        w.set_location(location);
        match w.surface() {
            Kind::Xdg(surface) => {
                let ret = surface.with_pending_state(|state| {
                    state.states.set(xdg_toplevel::State::TiledRight);
                    state.size = Some(size);
                });
                if ret.is_ok() {
                    surface.send_configure();
                }
            }
            Kind::Empty => {
                w.set_geometry(size);
            }
            Kind::Wl(_) => {} // Should we do something here?
        }
    }

    fn arrange_tiling(&mut self, log: Option<&slog::Logger>) {
        // println!("workspace {}'s size = {:?}", self.name, self.output_size);
        // println!("before tiling");
        let _ = log;
        // for w in &self.windows {
        //     println!("window: {}", w);
        // }
        let n_windows = self.windows.len() as i32;
        if n_windows == 0 {
            return;
        } else if n_windows == 1 {
            self.windows[0].set_location(&(0, 0));
            self.windows[0].set_geometry(self.output_size);
        }

        let mut y = 0;
        let mut x = 0;

        let mut size = self.output_size;
        let half_width = size.w / 2;
        let mut stack_height = size.h;
        if n_windows > 1 {
            x = half_width;
            size.w = half_width;
        }
        if n_windows > 2 {
            stack_height = size.h / (n_windows - 1);
        }
        self.windows[0].set_location(&(0, 0));

        match self.windows[0].surface() {
            Kind::Xdg(surface) => {
                let ret = surface.with_pending_state(|state| {
                    state.states.set(xdg_toplevel::State::TiledRight);
                    state.size = Some(size);
                });
                if ret.is_ok() {
                    surface.send_configure();
                }
            }
            Kind::Empty => {
                self.windows[0].set_geometry(size);
            }
            Kind::Wl(_) => {} // Should we do something here?
        }

        for window in &mut self.windows.iter_mut().skip(1) {
            window.set_location(&(x, y));
            if n_windows >= 1 {
                y += stack_height;
                size.h = stack_height;
            }
            match window.surface() {
                Kind::Xdg(surface) => {
                    let ret = surface.with_pending_state(|state| {
                        state.states.set(xdg_toplevel::State::TiledLeft);
                        state.states.set(xdg_toplevel::State::TiledBottom);
                        state.size = Some(size);
                    });
                    if ret.is_ok() {
                        surface.send_configure();
                    }
                }
                Kind::Empty => {
                    window.set_geometry(size);
                }
                Kind::Wl(_) => {} // Should we do something here?
            }
        }
        // println!("after tiling");
        // for w in &self.windows {
        //     println!("window: {}", w);
        // }
    }

    fn arrange_vert_tiling(&mut self, log: Option<&slog::Logger>) {
        let _ = log;
        let n_windows = self.windows.len() as i32;
        if n_windows == 0 {
            return;
        }
        let mut y = 0;
        let mut x = 0;

        let mut size = self.output_size;
        let half_height = size.h / 2;
        let mut stack_width = size.w;
        if n_windows > 1 {
            y = half_height;
            size.h = half_height;
        }

        if n_windows > 2 {
            stack_width = size.w / (n_windows - 1);
        }

        self.windows[0].set_location(&(0, 0));

        match self.windows[0].surface() {
            Kind::Xdg(surface) => {
                let ret = surface.with_pending_state(|state| {
                    state.states.set(xdg_toplevel::State::TiledRight);
                    state.size = Some(size);
                });
                if ret.is_ok() {
                    surface.send_configure();
                }
            }
            Kind::Empty => {
                self.windows[0].set_geometry(size);
            }
            Kind::Wl(_) => {} // Should we do something here?
        }

        for window in &mut self.windows.iter_mut().skip(1) {
            window.set_location(&(x, y));
            if n_windows >= 1 {
                x += stack_width;
                size.w = stack_width;
            }
            match window.surface() {
                Kind::Xdg(surface) => {
                    let ret = surface.with_pending_state(|state| {
                        state.states.set(xdg_toplevel::State::TiledLeft);
                        state.states.set(xdg_toplevel::State::TiledBottom);
                        state.size = Some(size);
                    });
                    if ret.is_ok() {
                        surface.send_configure();
                    }
                }
                Kind::Empty => {
                    window.set_geometry(size);
                }
                Kind::Wl(_) => {} // Should we do something here?
            }
        }
    }

    fn arrange_floating(&mut self, log: Option<&slog::Logger>) {
        let _ = log;
    }

    pub fn bring_surface_to_top(&mut self, surface: &WlSurface) {
        let found = self.windows.iter().enumerate().find(|(_, w)| {
            w.toplevel
                .get_surface()
                .map(|s| s.as_ref().equals(surface.as_ref()))
                .unwrap_or(false)
        });

        if let Some((id, _)) = found {
            self.bring_nth_window_to_top(id);
        }
    }

    pub fn next_window(&mut self) -> usize {
        if self.windows.len() == 0 {
            return self.current_window;
        }
        if self.current_window < self.windows.len() - 1 {
            self.current_window += 1;
        } else {
            self.current_window = 0;
        }
        if self.layout == Layout::Fullscreen {
            self.bring_nth_window_to_top(self.current_window);
        } else {
            self.focus_window(self.current_window);
        }
        self.current_window
    }

    pub fn prev_window(&mut self) -> usize {
        if self.windows.len() == 0 {
            return self.current_window;
        }
        if self.current_window != 0 {
            self.current_window -= 1;
        } else {
            self.current_window = self.windows.len() - 1;
        }
        if self.layout == Layout::Fullscreen {
            self.bring_nth_window_to_top(self.current_window);
        } else {
            self.focus_window(self.current_window);
        }
        self.current_window
    }

    pub fn move_prev_window(&mut self) -> usize {
        let mut next = self.current_window;
        if self.current_window != 0 {
            next -= 1;
        } else {
            next = self.windows.len() - 1;
        }
        self.move_window(self.current_window, next);
        self.current_window
    }

    pub fn move_next_window(&mut self) -> usize {
        let mut next = self.current_window;
        if self.current_window < self.windows.len() - 1 {
            next += 1;
        } else {
            next = 0;
        }
        self.move_window(self.current_window, next);
        self.current_window
    }

    fn move_window(&mut self, id: usize, next: usize) {
        let current = self.windows.swap_remove(id);
        self.windows.insert(next, current);
        self.current_window = next;
    }

    fn focus_window(&self, id: usize) {
        // Take activation away from all the windows
        for window in self.windows.iter() {
            window.toplevel.set_activated(false);
        }
        let window = &self.windows[id];
        window.toplevel.set_activated(true);
    }

    fn bring_nth_window_to_top(&mut self, id: usize) {
        // Moves windows
        let winner = self.windows.remove(id);

        // Take activation away from all the windows
        for window in self.windows.iter() {
            window.toplevel.set_activated(false);
        }

        // Give activation to our winner
        winner.toplevel.set_activated(true);
        self.windows.insert(0, winner);
    }

    pub fn get_surface_and_bring_to_top(
        &mut self,
        point: Point<f64, Logical>,
    ) -> Option<(wl_surface::WlSurface, Point<i32, Logical>)> {
        let mut found = None;
        for (i, w) in self.windows.iter().enumerate() {
            if let Some(surface) = w.matching(point) {
                found = Some((i, surface));
                break;
            }
        }
        if let Some((id, surface)) = found {
            self.bring_nth_window_to_top(id);
            Some(surface)
        } else {
            None
        }
    }

    pub fn get_surface_under(
        &self,
        point: Point<f64, Logical>,
    ) -> Option<(wl_surface::WlSurface, Point<i32, Logical>)> {
        if let Some(res) = self.layers.get_surface_under(&Layer::Overlay, point) {
            return Some(res);
        }
        if let Some(res) = self.layers.get_surface_under(&Layer::Top, point) {
            return Some(res);
        }

        for w in &self.windows {
            if let Some(surface) = w.matching(point) {
                return Some(surface);
            }
        }

        if let Some(res) = self.layers.get_surface_under(&Layer::Bottom, point) {
            return Some(res);
        }
        if let Some(res) = self.layers.get_surface_under(&Layer::Background, point) {
            return Some(res);
        }

        None
    }

    pub fn with_child_popups<Func>(&self, base: &wl_surface::WlSurface, mut f: Func)
    where
        Func: FnMut(&PopupKind),
    {
        for w in self
            .popups
            .iter()
            .rev()
            .filter(move |w| w.popup.parent().as_ref() == Some(base))
        {
            f(&w.popup)
        }
    }

    pub fn with_windows_from_bottom_to_top<Func>(&self, mut f: Func)
    where
        Func: FnMut(&Kind, Point<i32, Logical>, &Rectangle<i32, Logical>),
    {
        for w in self.windows.iter().rev() {
            f(&w.toplevel, w.location, &w.bbox)
        }
    }
}
