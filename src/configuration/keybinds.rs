use serde::Deserialize;
use anyhow::bail;
use smithay::{wayland::seat::ModifiersState, utils::{Physical, Point, Logical, Size}};
use crate::{
    keybind::{Action, Direction, KeyAction, Keybind},
    workspace::workspace,
};

#[derive(Debug, PartialEq, Deserialize)]
pub struct Keybinds
{
    pub spawn: Vec<(String, String)>,
    pub window: Vec<(String, String)>,
    pub workspace: Vec<(String, String)>,
    pub wm: Vec<(String, String)>,
}

impl std::fmt::Display for Keybinds
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        for kb in self.get()
        {
            write!(f, "{}\n", kb)?;
        }
        Ok(())
    }
}

impl std::default::Default for Keybinds
{
    fn default() -> Self {
        Keybinds
        {
            spawn: vec![("alacritty".into(), "super + enter".into())],
            window: vec![
                ("next_window".into(), "super + j".into()),
                ("prev_window".into(), "super + k".into()),
                ("next_output".into(), "super + .".into()),
                ("prev_output".into(), "super + ,".into()),
                ("move_next_window".into(), "super + shift + J".into()),
                ("move_prev_window".into(), "super + shift + K".into()),
                ("move_next_output".into(), "super + shift + >".into()),
                ("move_prev_output".into(), "super + shift + <".into()),
                ("close".into(), "super + q".into()),
                ("set_layout_tiling".into(), "super + t".into()),
                ("set_layout_vert_tiling".into(), "super + shift + T".into()),
                ("set_layout_float".into(), "super + shift + F".into()),
                ("set_layout_fullscreen".into(), "super + f".into()),
                ("show_all_windows".into(), "super + a".into()),
            ],
            wm: vec![
                    ("reload".into(), "super + ctrl + r".into()),
                    ("quit".into(), "super + ctrl + q".into()),
                ],
            
            workspace: vec![
            ("1".into(), "super + 1".into()),
            ("2".into(), "super + 2".into()),
            ("3".into(), "super + 3".into()),
            ("4".into(), "super + 4".into()),
            ("5".into(), "super + 5".into()),
            ("6".into(), "super + 6".into()),
            ("7".into(), "super + 7".into()),
            ("8".into(), "super + 8".into()),
            ("9".into(), "super + 9".into()),
            ("move_1".into(), "super + shift + 1".into()),
            ("move_2".into(), "super + shift + 2".into()),
            ("move_3".into(), "super + shift + 3".into()),
            ("move_4".into(), "super + shift + 4".into()),
            ("move_5".into(), "super + shift + 5".into()),
            ("move_6".into(), "super + shift + 6".into()),
            ("move_7".into(), "super + shift + 7".into()),
            ("move_8".into(), "super + shift + 8".into()),
            ("move_9".into(), "super + shift + 9".into()),
            ],
        }
    }
}

impl Keybinds
{
    // TODO: Maybe return an option instead
    fn to_action(s: &str) -> KeyAction
    {
        use Action::*;
        use Direction::*;
        use KeyAction::*;

        match s
        {
            "next_window" => Window(Focus, Next, false),
            "prev_window" => Window(Focus, Prev, false),
            "move_next_window" => Window(Move, Next, false),
            "move_prev_window" => Window(Move, Prev, false),
            "close" => Close,
            "reload" => Reload,
            "quit" => Quit,
            "next_output" => Output(Next),
            "prev_output" => Output(Prev),
            "move_next_output" => Window(Move, Next, true),
            "move_prev_output" => Window(Move, Prev, true),
            "set_layout_tiling" => Layout(workspace::Layout::Tiling),
            "set_layout_vert_tiling" => Layout(workspace::Layout::VertTiling),
            "set_layout_float" => Layout(workspace::Layout::Floating),
            "set_layout_fullscreen" => Layout(workspace::Layout::Fullscreen),
            "show_all_windows" => ShowAll,
            x if x.starts_with("move_") =>
            {
                let s = &x["move_".len()..];
                if let Ok(val) = u8::from_str_radix(s, 10)
                {
                    Workspace(Move, val)
                }
                else
                {
                    Run(s.into())
                }
            }

            _ =>
            {
                // any digit would be a Workspace
                if let Ok(val) = u8::from_str_radix(s, 10)
                {
                    Workspace(Focus, val)
                }
                // anything else would be a Run
                else
                {
                    Run(s.into())
                }
            }
        }
    }

    fn as_vec(&self) -> Vec<&(String, String)>
    {
        let mut keybinds = Vec::new();
        keybinds.extend(self.wm.iter());
        keybinds.extend(self.spawn.iter());
        keybinds.extend(self.window.iter());
        keybinds.extend(self.workspace.iter());
        keybinds
    }

    pub fn get(&self) -> Vec<Keybind>
    {
        let mut keybinds = Vec::new();
        for keybind in self.as_vec()
        {
            let (cmd, kb) = keybind;
            let splitted_kb: Vec<&str> = kb.split(" + ").collect();
            let kb = Keybind::new(splitted_kb, Some(Keybinds::to_action(cmd)));
            keybinds.push(kb);
        }
        keybinds
    }
}

pub fn to_size(s: impl AsRef<str>) -> anyhow::Result<Size<i32, Physical>>
{
    let s = s.as_ref();
    match s.split_once("x")
    {
        Some((first , second))=>Ok((first.parse::<i32>()?, second.parse::<i32>()?).into()),
        None=>bail!("Oh no! We found a size that did not have an 'x' to separate the numbers! Whelp!"),
    }
}

pub fn to_point(s: impl AsRef<str>) -> anyhow::Result<Point<i32, Logical>>
{
    let s = s.as_ref();
    match s.split_once("x")
    {
        Some((first , second)) => Ok((first.parse::<i32>()?, second.parse::<i32>()?).into()),
        None => bail!("Oh no! We found a point that did not have an 'x' to separate the numbers! Whelp!"),
    }
}
#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn parsing_size()
    {
        let first = to_size("1920x1080").expect("This shouldn't have failed");
        let exp = (1920,1080).into();
        assert_eq!(first, exp);
        let second = to_size("1238417234812341234--112341234");
        assert!(second.is_err());
    }

    // #[test]
    // fn keybinds()
    // {
    //     let cfg = crate::configuration::configuration::get_config(None);
    //     let kb = cfg.keybinds.get();
    //     let expected = [
    //         Keybind::new(vec!["super", "ctrl", "r"], Some(KeyAction::Reload)),
    //         Keybind::new(vec!["super", "ctrl", "q"], Some(KeyAction::Quit)),
    //         Keybind::new(vec!["super", "b"], Some(KeyAction::Run("$BROWSER".into()))),
    //         Keybind::new(
    //             vec!["super", "Return"],
    //             Some(KeyAction::Run("$TERMINAL".into())),
    //         ),
    //         Keybind::new(
    //             vec!["super", "j"],
    //             Some(KeyAction::Window(Action::Focus, Direction::Next, false)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "k"],
    //             Some(KeyAction::Window(Action::Focus, Direction::Prev, false)),
    //         ),
    //         Keybind::new(vec!["super", "."], Some(KeyAction::Output(Direction::Next))),
    //         Keybind::new(vec!["super", ","], Some(KeyAction::Output(Direction::Prev))),
    //         Keybind::new(
    //             vec!["super", "shift", "J"],
    //             Some(KeyAction::Window(Action::Move, Direction::Next, false)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "K"],
    //             Some(KeyAction::Window(Action::Move, Direction::Prev, false)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", ">"],
    //             Some(KeyAction::Window(Action::Move, Direction::Next, true)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "<"],
    //             Some(KeyAction::Window(Action::Move, Direction::Prev, true)),
    //         ),
    //         Keybind::new(vec!["super", "q"], Some(KeyAction::Close)),
    //         Keybind::new(
    //             vec!["super", "t"],
    //             Some(KeyAction::Layout(workspace::Layout::Tiling)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "T"],
    //             Some(KeyAction::Layout(workspace::Layout::VertTiling)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "F"],
    //             Some(KeyAction::Layout(workspace::Layout::Floating)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "f"],
    //             Some(KeyAction::Layout(workspace::Layout::Fullscreen)),
    //         ),
    //         Keybind::new(vec!["super", "a"], Some(KeyAction::ShowAll)),
    //         Keybind::new(
    //             vec!["super", "1"],
    //             Some(KeyAction::Workspace(Action::Focus, 1)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "2"],
    //             Some(KeyAction::Workspace(Action::Focus, 2)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "3"],
    //             Some(KeyAction::Workspace(Action::Focus, 3)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "4"],
    //             Some(KeyAction::Workspace(Action::Focus, 4)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "5"],
    //             Some(KeyAction::Workspace(Action::Focus, 5)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "6"],
    //             Some(KeyAction::Workspace(Action::Focus, 6)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "7"],
    //             Some(KeyAction::Workspace(Action::Focus, 7)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "8"],
    //             Some(KeyAction::Workspace(Action::Focus, 8)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "9"],
    //             Some(KeyAction::Workspace(Action::Focus, 9)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "1"],
    //             Some(KeyAction::Workspace(Action::Move, 1)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "2"],
    //             Some(KeyAction::Workspace(Action::Move, 2)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "3"],
    //             Some(KeyAction::Workspace(Action::Move, 3)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "4"],
    //             Some(KeyAction::Workspace(Action::Move, 4)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "5"],
    //             Some(KeyAction::Workspace(Action::Move, 5)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "6"],
    //             Some(KeyAction::Workspace(Action::Move, 6)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "7"],
    //             Some(KeyAction::Workspace(Action::Move, 7)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "8"],
    //             Some(KeyAction::Workspace(Action::Move, 8)),
    //         ),
    //         Keybind::new(
    //             vec!["super", "shift", "9"],
    //             Some(KeyAction::Workspace(Action::Move, 9)),
    //         ),
    //     ];
    //     for (a, b) in kb.into_iter().zip(expected)
    //     {
    //         assert_eq!(a, b);
    //     }
    // }
    // #[test]
    // fn keybinds_to_vec()
    // {
    //     let cfg = crate::configuration::configuration::get_config(None);
    //     let vec_of_keybinds = cfg.keybinds.as_vec();
    //
    //     let expected: Vec<(String, String)> = vec![
    //         ("reload".into(), "super + ctrl + r".into()),
    //         ("quit".into(), "super + ctrl + q".into()),
    //         ("$BROWSER".into(), "super + b".into()),
    //         ("$TERMINAL".into(), "super + Return".into()),
    //         ("next_window".into(), "super + j".into()),
    //         ("prev_window".into(), "super + k".into()),
    //         ("next_output".into(), "super + .".into()),
    //         ("prev_output".into(), "super + ,".into()),
    //         ("move_next_window".into(), "super + shift + J".into()),
    //         ("move_prev_window".into(), "super + shift + K".into()),
    //         ("move_next_output".into(), "super + shift + >".into()),
    //         ("move_prev_output".into(), "super + shift + <".into()),
    //         ("close".into(), "super + q".into()),
    //         ("set_layout_tiling".into(), "super + t".into()),
    //         ("set_layout_vert_tiling".into(), "super + shift + T".into()),
    //         ("set_layout_float".into(), "super + shift + F".into()),
    //         ("set_layout_fullscreen".into(), "super + f".into()),
    //         ("show_all_windows".into(), "super + a".into()),
    //         ("1".into(), "super + 1".into()),
    //         ("2".into(), "super + 2".into()),
    //         ("3".into(), "super + 3".into()),
    //         ("4".into(), "super + 4".into()),
    //         ("5".into(), "super + 5".into()),
    //         ("6".into(), "super + 6".into()),
    //         ("7".into(), "super + 7".into()),
    //         ("8".into(), "super + 8".into()),
    //         ("9".into(), "super + 9".into()),
    //         ("move_1".into(), "super + shift + 1".into()),
    //         ("move_2".into(), "super + shift + 2".into()),
    //         ("move_3".into(), "super + shift + 3".into()),
    //         ("move_4".into(), "super + shift + 4".into()),
    //         ("move_5".into(), "super + shift + 5".into()),
    //         ("move_6".into(), "super + shift + 6".into()),
    //         ("move_7".into(), "super + shift + 7".into()),
    //         ("move_8".into(), "super + shift + 8".into()),
    //         ("move_9".into(), "super + shift + 9".into()),
    //     ];
    //
    //     assert_eq!(vec_of_keybinds.len(), expected.len());
    //     for (a, b) in vec_of_keybinds.into_iter().zip(expected)
    //     {
    //         assert_eq!(a, &b);
    //     }
    // }
}
