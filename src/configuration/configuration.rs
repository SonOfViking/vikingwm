// use anyhow::Result;
use serde::Deserialize;
use slog::Logger;
// use smithay::wayland::seat::{Keysym, ModifiersState};
use smithay::{wayland::seat::ModifiersState, utils::{Physical, Point, Logical, Size}};
use std::{env, fs};
use crate::configuration::keybinds::Keybinds;
use std::default;
use std::path::Path;

#[derive(Debug, Deserialize, PartialEq)]
pub struct Config
{
    pub startup: Vec<String>,
    pub screen: Screen,
    pub keybinds: Keybinds,
}


// TODO: Think about what to do when there is only 1 output, what should be optional here? Or
// should the outputs vec be optional?
#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct Output
{
    pub name: String,
    pub resolution: String,
    pub location: String,
    pub rotation: u8,
    pub refresh: i32,
    pub layout: String,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct Screen
{
    pub outputs: Vec<Output>,
    pub workspaces: Vec<String>,
}

impl default::Default for Screen
{
    fn default() -> Self { Screen{outputs: Vec::new(), workspaces : vec!["1".into(), "2".into(), "3".into(), "4".into(), "5".into(), "6".into(), "7".into(), "8".into(), "9".into(), "0".into()] }}
}
impl default::Default for Config
{
    fn default() -> Self { Config { startup: Vec::new(), screen: Default::default(), keybinds: Default::default() } }
}

#[derive(Debug)]
struct TestErr;


impl std::fmt::Display for Config
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "Screen:{}\nKeybinds:{}\n", &self.screen, &self.keybinds)
    }
}

impl std::fmt::Display for Output
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(
            f,
            "name: {}, resolution: {}, location: {}, rotation:{}, refresh:{}, layout:{}",
            &self.name,
            &self.resolution,
            &self.location,
            &self.rotation,
            &self.refresh,
            &self.layout
        )
    }
}

impl std::fmt::Display for Screen
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "outputs:\n")?;
        for o in &self.outputs
        {
            write!(f, "{}\n", o)?;
        }
        write!(f, "workspaces: [")?;
        for ws in &self.workspaces
        {
            write!(f, "{},", ws)?;
        }
        write!(f, "]")
    }
}


fn new_empty_modstate() -> ModifiersState
{
    ModifiersState {
        logo: false,
        alt: false,
        caps_lock: false,
        ctrl: false,
        shift: false,
        num_lock: false,
    }
}

pub fn new_modstate(keys: Vec<&str>) -> ModifiersState
{
    let mut mods = new_empty_modstate();

    for current in keys
    {
        dbg!(&::xkbcommon::xkb::keysym_get_name(
            ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS)
        ));
        let key = ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS);
        if key == xkbcommon::xkb::KEY_NoSymbol
        {
            match current
            {
                #[cfg(feature = "debug")]
                "super" => mods.alt = true,
                #[cfg(not(feature = "debug"))]
                "super" => mods.logo = true,
                "ctrl" => mods.ctrl = true,
                "shift" => mods.shift = true,
                "num_lock" => mods.num_lock = true,
                "caps_lock" => mods.caps_lock = true,
                _ =>
                {} // xkbcommon::xkb::keysym_from_name(&m, xkbcommon::xkb::KEYSYM_NO_FLAGS)
            }
        }
    }
    mods
}

// TODO: better error handling
fn parse_config(path: &str) -> Config
{
    let config = fs::read_to_string(path).expect("Could not find the configuration file");
    toml::from_str(&config).expect("Could not parse configuation")
}

#[cfg(feature = "debug")]
pub fn get_config(logger: Option<&Logger>) -> Config
{
    let _ = logger;
    parse_config("debug_config.toml")
}

#[cfg(not(feature = "debug"))]
// get path: $XDG_CONFIG_HOME/vikingwm/config.toml
// TODO: copy default_config to path if not there available yet
pub fn get_config(logger: Option<&Logger>) -> Config
{
    let xdg_set = env::var("XDG_CONFIG_HOME");
    let config_path;
    if xdg_set.is_ok()
    {
        config_path = format!("{}/vikingwm/config.toml", &xdg_set.unwrap());
    }
    else
    {
        let home = env::var("HOME").expect("YOU DON'T HAVE THE HOME ENVVAR SET?! YOU MONSTER!");
        config_path = format!("{}/.config/vikingwm/config.toml", home);
    };

    if !Path::new(&config_path).exists()
    {
        if let Some(log) = logger
        {
            slog::info!(log, "Using default configuration");
        }
        return Default::default();
    }
    parse_config(&config_path)
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn no_config()
    {
        let cfg = get_config(None);
        let actual : Config = Default::default();
        assert_eq!(cfg, actual);
    }
}
