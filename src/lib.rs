#![warn(rust_2018_idioms)]
// If no backend is enabled, a large portion of the codebase is unused.
// So silence this useless warning for the CI.
#![cfg_attr(
    not(any(feature = "debug", feature = "release")),
    allow(dead_code, unused_imports)
)]

#[macro_use]
extern crate slog;

pub mod backend;
pub mod configuration;
pub mod input;
pub mod keybind;
pub mod output;
pub mod state;
pub mod workspace;
