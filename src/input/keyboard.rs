use crate::{
    configuration::configuration::get_config,
    keybind::{Action, Direction, KeyAction, Keybind},
    state::Vikingwm,
    workspace::window_map::Kind,
};

use slog::Logger;
use smithay::{
    // backend::input::{Event, InputBackend, InputEvent, KeyState, KeyboardKeyEvent},
    backend::input::{Event, InputBackend, KeyState, KeyboardKeyEvent},
    // reexports::wayland_server::{
        // protocol::wl_pointer, protocol::wl_surface::WlSurface, Client, Resource,
    // },
    wayland::{
        // seat::{keysyms as xkb, AxisFrame, FilterResult, KeyboardHandle, Keysym, ModifiersState},
        seat::{FilterResult, KeyboardHandle, Keysym, ModifiersState},
        SERIAL_COUNTER as SCOUNTER,
    },
};
use std::{process::Command, sync::atomic::Ordering};

fn output_name() -> &'static str
{
    #[cfg(feature = "debug")]
    return crate::backend::winit::OUTPUT_NAME;
    #[cfg(not(feature = "debug"))]
    return "";
}

impl<Backend> Vikingwm<Backend>
{
    /// set keyboard focus to the program
    // fn give_keyboard_focus(&self, win: &Window)
    // {
    //     let surface = win.surface();
    //     if let Kind::Xdg(wl) = surface
    //     {
    //         let serial = SCOUNTER.next_serial();
    //         self.keyboard.set_focus(wl.get_surface(), serial);
    //     }
    // }

    pub fn handle_keyboard<B: InputBackend>(&mut self, event: B::KeyboardKeyEvent)
    {
        let kbs = self.keybinds.clone();
        let key_action = Vikingwm::<B>::keyboard_key_to_action::<B>(
            &mut self.keyboard.clone(),
            &kbs,
            event,
            &mut self.suppressed_keys,
            Some(&self.log),
        );
        self.handle_keyaction(&key_action);
    }

    fn keyboard_key_to_action<'a, B: InputBackend>(
        keyboard: &mut KeyboardHandle,
        keybinds: &'a Vec<Keybind>,
        evt: B::KeyboardKeyEvent,
        suppressed_keys: &mut Vec<Keysym>,
        log: Option<&Logger>,
    ) -> &'a KeyAction
    {
        let keycode = evt.key_code();
        let state = evt.state();
        if let Some(log) = log
        {
            debug!(log, "key"; "keycode" => keycode, "state" => format!("{:?}", state));
        }
        let serial = SCOUNTER.next_serial();
        let time = Event::time(&evt);
        keyboard
            .input(keycode, state, serial, time, |modifiers, handle| {
                let keysym = handle.modified_sym();

                if let Some(log) = log
                {
                    slog::debug!(log, "keysym";
                        "state" => format!("{:?}", state),
                        "mods" => format!("{:?}", modifiers),
                        "keysym" => ::xkbcommon::xkb::keysym_get_name(keysym)
                    );
                }

                // If the key is pressed and triggered a action
                // we will not forward the key to the client.
                // Additionally add the key to the suppressed keys
                // so that we can decide on a release if the key
                // should be forwarded to the client or not.
                if let KeyState::Pressed = state
                {
                    let action = process_keyboard_shortcut(keybinds, modifiers, &keysym, log);

                    if action.is_some()
                    {
                        suppressed_keys.push(keysym);
                    }

                    if let Some(log) = log
                    {
                        debug!(log, "the action was {:?}", action);
                    }

                    let after_map = action
                        .map(FilterResult::Intercept)
                        .unwrap_or(FilterResult::Forward);
                    if let Some(log) = log
                    {
                        debug!(log, "the action is now {:?}", after_map);
                    }
                    return after_map;
                }
                else
                {
                    let suppressed = suppressed_keys.contains(&keysym);
                    if suppressed
                    {
                        suppressed_keys.retain(|k| *k != keysym);
                        FilterResult::Intercept(&KeyAction::None)
                    }
                    else
                    {
                        FilterResult::Forward
                    }
                }
            })
            .unwrap_or(&KeyAction::None)
    }

    // TODO: What to do with winit::OUTPUT_NAME?
    fn handle_keyaction(&mut self, key_action: &KeyAction)
    {
        match key_action
        {
            KeyAction::None =>
            {}
            KeyAction::Quit =>
            {
                info!(self.log, "Quitting.");
                self.running.store(false, Ordering::SeqCst);
            }
            KeyAction::Reload =>
            {
                warn!(self.log, "Action 'Reload' not implemented.");
                self.config = get_config(Some(&self.log));
            }
            KeyAction::Window(act, dir, to_other_output) =>
            {
                let ws = &mut self.workspaces[self.current_ws];
                if *to_other_output == true
                {
                    // TODO: Implement moving to different outputs
                    slog::warn!(self.log, "Moving to another output is not implented yet");
                }
                // TODO: How do we get the correct window on top if layout is Fullscreen?
                match (act, dir)
                {
                    (Action::Focus, Direction::Next) =>
                    {
                        let _ = ws.next_window();
                        //slog::debug!(
                        //    self.log,
                        //    "Selecting next window, currently selected idx = {}", idx
                        //);
                    }
                    (Action::Focus, Direction::Prev) =>
                    {
                        let _ = ws.prev_window();
                        // debug!(
                        //     self.log,
                        //     "Selecting next window, currently selected idx = {}", idx
                        // );
                    }
                    (Action::Move, Direction::Next) =>
                    {
                        let _ = ws.move_next_window();
                        // debug!(
                        //     self.log,
                        //     "Moving next window, currently selected idx = {}", idx
                        // );
                    }
                    (Action::Move, Direction::Prev) =>
                    {
                        let _ = ws.move_prev_window();
                        // debug!(
                        //     self.log,
                        //     "Moving prev window, currently selected idx = {}", idx
                        // );
                    }
                    // TODO: Implement moving directionally
                    _ => warn!(
                        self.log,
                        "Directional window focussing is not implemented yet"
                    ),
                }

                if let Some(win) = ws.current_window()
                {
                    let surface = win.surface();
                    if let Kind::Xdg(wl) = surface
                    {
                        let serial = SCOUNTER.next_serial();
                        self.keyboard.set_focus(wl.get_surface(), serial);
                    }
                }
                // self.give_keyboard_focus(win);
            }
            KeyAction::Close =>
            {
                let ws = &mut self.workspaces[self.current_ws];
                ws.close_current();
                // if ws.windows().len() == 0
                // {
                //     return;
                // }
                // if let Some(current_window) = ws.current_window()
                // {
                //     if let Kind::Xdg(sfc) = current_window.surface()
                //     {
                //         sfc.send_close();
                //     }
                // }
                // if ws.current_window > 0
                // {
                //     ws.current_window -= 1;
                // }
                // else
                // {
                //     ws.current_window = 0;
                // }

                // info!(self.log, "Closing window");

                // let surface = ws.current_window().unwrap().surface();
                if let Some(surface) = ws.current_window()
                {
                    if let Kind::Xdg(wl) = surface.surface()
                    {
                        let serial = SCOUNTER.next_serial();
                        self.keyboard.set_focus(wl.get_surface(), serial);
                    }
                }
            }
            KeyAction::Output(_) =>
            {
                warn!(self.log, "Action 'Output' not implemented.");
            }
            KeyAction::Workspace(act, ws_idx) => match act
            {
                Action::Focus =>
                {
                    let ws_idx = *ws_idx as usize;
                    if ws_idx > 0 && ws_idx <= self.workspaces.len()
                    {
                        self.current_ws = ws_idx - 1;
                        let ws = &self.workspaces[self.current_ws];
                        info!(
                            self.log,
                            "Switching to the {} workspace (idx = {})", ws.name, self.current_ws
                        );
                    }
                }
                Action::Move =>
                {
                    self.move_window_to_workspace(*ws_idx as usize);
                }
            },
            KeyAction::Screen(_) =>
            {
                warn!(self.log, "Action 'Screen' not implemented.");
            }
            KeyAction::VtSwitch(_) =>
            {
                warn!(self.log, "Action 'VTSwitch' not implemented.");
            }
            KeyAction::Run(cmd) =>
            {
                // Parse Environment variables here, so that if they are changed while running the
                // wm, they are still correct

                let mut command = cmd.to_owned();
                if let Some(env_var) = cmd.strip_prefix('$')
                {
                    if let Ok(var) = std::env::var(env_var)
                    {
                        command = var;
                    }
                }
                info!(self.log, "Starting program"; "cmd" => cmd.clone());
                if let Err(e) = Command::new(command).spawn()
                {
                    error!(self.log,
                        "Failed to start program";
                        "cmd" => cmd,
                        "err" => format!("{:?}", e)
                    );
                }
            }
            KeyAction::ScaleUp =>
            {
                let current_scale = {
                    self.output_map
                        .borrow()
                        .find_by_name(output_name())
                        .map(|o| o.scale())
                        .unwrap_or(1.0)
                };
                self.output_map
                    .borrow_mut()
                    .update_scale_by_name(current_scale + 0.25f32, output_name());
            }
            KeyAction::ScaleDown =>
            {
                let current_scale = {
                    self.output_map
                        .borrow()
                        .find_by_name(output_name())
                        .map(|o| o.scale())
                        .unwrap_or(1.0)
                };

                self.output_map
                    .borrow_mut()
                    .update_scale_by_name(f32::max(1.0f32, current_scale - 0.25f32), output_name());
            }
            KeyAction::Layout(new_layout) =>
            {
                let ws = &mut self.workspaces[self.current_ws];
                ws.layout(new_layout.to_owned());
            }
            KeyAction::ShowAll =>
            {
                self.show_all();
            }
            // action =>
            // {
            //     warn!(
            //         self.log,
            //         "Key action {:?} unsupported on winit backend.", action
            //     );
            // }
        }
    }
}

fn process_keyboard_shortcut<'a>(
    keybinds: &'a Vec<Keybind>,
    modifiers: &ModifiersState,
    keysym: &Keysym,
    log: Option<&Logger>,
) -> Option<&'a KeyAction>
{
    let input = Keybind::from_kb(modifiers, keysym);
    let current = keybinds.into_iter().find(|kb| **kb == input);
    if let Some(log) = log
    {
        debug!(log, "currently found keybind = {:?}", current);
    }
    current.map(|val| val.action())
}
