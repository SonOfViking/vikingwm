use crate::state::Vikingwm;
use smithay::{
    backend::input::{
        self, Event, InputBackend, PointerAxisEvent, PointerButtonEvent, PointerMotionAbsoluteEvent,
    },
    reexports::wayland_server::protocol::wl_pointer,
    wayland::{seat::AxisFrame, SERIAL_COUNTER as SCOUNTER},
};

fn output_name() -> &'static str
{
    #[cfg(feature = "debug")]
    return crate::backend::winit::OUTPUT_NAME;
    #[cfg(not(feature = "debug"))]
    return "";
}

impl<Backend> Vikingwm<Backend>
{
    pub fn on_pointer_button<B: InputBackend>(&mut self, evt: B::PointerButtonEvent)
    {
        let serial = SCOUNTER.next_serial();
        let button = evt.button_code();
        let state = match evt.state()
        {
            input::ButtonState::Pressed =>
            {
                // change the keyboard focus unless the pointer is grabbed
                if !self.pointer.is_grabbed()
                {
                    let under = self
                        .workspaces[self.current_ws]
                        .get_surface_and_bring_to_top(self.pointer_location);
                    self.keyboard
                        .set_focus(under.as_ref().map(|&(ref s, _)| s), serial);
                }
                wl_pointer::ButtonState::Pressed
            }
            input::ButtonState::Released => wl_pointer::ButtonState::Released,
        };
        self.pointer.button(button, state, serial, evt.time());
    }

    pub fn on_pointer_axis<B: InputBackend>(&mut self, evt: B::PointerAxisEvent)
    {
        let source = match evt.source()
        {
            input::AxisSource::Continuous => wl_pointer::AxisSource::Continuous,
            input::AxisSource::Finger => wl_pointer::AxisSource::Finger,
            input::AxisSource::Wheel | input::AxisSource::WheelTilt =>
            {
                wl_pointer::AxisSource::Wheel
            }
        };
        let horizontal_amount = evt
            .amount(input::Axis::Horizontal)
            .unwrap_or_else(|| evt.amount_discrete(input::Axis::Horizontal).unwrap() * 3.0);
        let vertical_amount = evt
            .amount(input::Axis::Vertical)
            .unwrap_or_else(|| evt.amount_discrete(input::Axis::Vertical).unwrap() * 3.0);
        let horizontal_amount_discrete = evt.amount_discrete(input::Axis::Horizontal);
        let vertical_amount_discrete = evt.amount_discrete(input::Axis::Vertical);

        {
            let mut frame = AxisFrame::new(evt.time()).source(source);
            if horizontal_amount != 0.0
            {
                frame = frame.value(wl_pointer::Axis::HorizontalScroll, horizontal_amount);
                if let Some(discrete) = horizontal_amount_discrete
                {
                    frame = frame.discrete(wl_pointer::Axis::HorizontalScroll, discrete as i32);
                }
            }
            else if source == wl_pointer::AxisSource::Finger
            {
                frame = frame.stop(wl_pointer::Axis::HorizontalScroll);
            }
            if vertical_amount != 0.0
            {
                frame = frame.value(wl_pointer::Axis::VerticalScroll, vertical_amount);
                if let Some(discrete) = vertical_amount_discrete
                {
                    frame = frame.discrete(wl_pointer::Axis::VerticalScroll, discrete as i32);
                }
            }
            else if source == wl_pointer::AxisSource::Finger
            {
                frame = frame.stop(wl_pointer::Axis::VerticalScroll);
            }
            self.pointer.axis(frame);
        }
    }

    pub fn on_pointer_move_absolute<B: InputBackend>(&mut self, evt: B::PointerMotionAbsoluteEvent)
    {
        let output_size = self
            .output_map
            .borrow()
            .find_by_name(output_name())
            .map(|o| o.size())
            .unwrap();
        let pos = evt.position_transformed(output_size);
        self.pointer_location = pos;
        let serial = SCOUNTER.next_serial();
        let under = self.workspaces[self.current_ws].get_surface_under(pos);
        self.pointer.motion(pos, under, serial, evt.time());
    }
}
