use crate::configuration::{keybinds, configuration};
use smithay::{
    utils::{Logical, Point},
    wayland::output::Mode,
    reexports::wayland_server::protocol::wl_output,
};
use anyhow::bail;

pub struct OutputState
{
    pub mode: Mode,
    pub transform: wl_output::Transform,
    pub output_scale: i32,
    pub location: Point<i32, Logical>,
}


impl OutputState
{
    pub fn new(cfg: &configuration::Output) -> anyhow::Result<OutputState>
    {
        let mode = Mode {
            size: keybinds::to_size(&cfg.resolution)?,
            refresh: cfg.refresh,
        };

        let scale = std::env::var(format!("ANVIL_SCALE_{}", cfg.name))
            .ok()
            .and_then(|s| s.parse::<f32>().ok())
            .unwrap_or(1.0)
            .max(1.0);

        let output_scale = scale.round() as i32;
        let location = keybinds::to_point(&cfg.location)?;
        //     let physical = PhysicalProperties
        //                    {
        //                         size: (0, 0).into(),
        //                         subpixel: wl_output::Subpixel::Unknown,
        //                         make: "Smithay".into(),
        //                         model: "Winit".into(),
        //                    };
        let transform = match wl_output::Transform::from_raw(cfg.rotation as u32)
        {
            Some(t) => t,
            None => bail!("oh no, could not get tranformation from cfg"),
        };
        Ok(OutputState
        {
            mode,
            transform,
            output_scale,
            location,
        })

    }
}
