use std::{
    cell::RefCell,
    rc::Rc,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
    process::Command,
};

use smithay::{
    reexports::{
        calloop::{generic::Generic, Interest, LoopHandle, Mode, PostAction},
        wayland_protocols::unstable::xdg_decoration,
        // wayland_protocols::unstable::xdg_shell::server::xdg_toplevel::State,
        wayland_server::{protocol::wl_surface::WlSurface, Display},
    },
    utils::{Logical, Point},
    wayland::{
        data_device::{
            default_action_chooser, init_data_device, set_data_device_focus, DataDeviceEvent,
        },
        output::xdg::init_xdg_output_manager,
        seat::{CursorImageStatus, KeyboardHandle, PointerHandle, Seat, XkbConfig},
        shell::xdg::decoration::{init_xdg_decoration_manager, XdgDecorationRequest},
        shm::init_shm_global,
        xdg_activation::{init_xdg_activation_global, XdgActivationEvent},
    },
};

#[cfg(feature = "xwayland")]
use smithay::xwayland::{XWayland, XWaylandEvent};

use crate::{
    backend::{shell::init_shell, shell::ShellHandles},
    configuration::configuration::Config,
    keybind::Keybind,
    output::output_map::OutputMap,
    workspace::window_map::Window,
    workspace::workspace::Workspace,
};

#[derive(Debug)]
pub struct Vikingwm<BackendData>
{
    pub config: Config,
    pub backend_data: BackendData,
    pub socket_name: Option<String>,
    pub running: Arc<AtomicBool>,
    pub display: Rc<RefCell<Display>>,
    pub handle: LoopHandle<'static, Vikingwm<BackendData>>,
    pub workspaces: Vec<Workspace>,
    pub current_ws: usize,
    pub output_map: Rc<RefCell<crate::output::output_map::OutputMap>>,
    pub dnd_icon: Arc<Mutex<Option<WlSurface>>>,
    pub log: slog::Logger,
    // input-related fields
    pub pointer: PointerHandle,
    pub keyboard: KeyboardHandle,
    pub suppressed_keys: Vec<u32>,
    pub pointer_location: Point<f64, Logical>,
    pub cursor_status: Arc<Mutex<CursorImageStatus>>,
    pub seat_name: String,
    pub seat: Seat,
    pub start_time: std::time::Instant,
    pub shell_states: ShellHandles,
    pub keybinds: Vec<Keybind>,
    // things we must keep alive
    #[cfg(feature = "xwayland")]
    pub xwayland: XWayland<Vikingwm<BackendData>>,
}

impl<BackendData> std::fmt::Display for Vikingwm<BackendData>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "workspaces:\n")?;
        for ws in &self.workspaces
        {
            write!(f, "{}\n", ws)?;
        }
        write!(f, "current_ws: {}", self.current_ws + 1)?; // Adding 1 so it's name is displayed properly
        write!(f, " outputs: {}", self.output_map.borrow())
    }
}

impl<BackendData: Backend + 'static + std::fmt::Debug> Vikingwm<BackendData>
{
    pub fn init(
        config: Config,
        display: Rc<RefCell<Display>>,
        handle: LoopHandle<'static, Vikingwm<BackendData>>,
        backend_data: BackendData,
        listen_on_socket: bool,
        log: Option<&slog::Logger>,
    ) -> anyhow::Result<Vikingwm<BackendData>>
    {
        // init the wayland connection
        handle
            .insert_source(
                Generic::from_fd(display.borrow().get_poll_fd(), Interest::READ, Mode::Level),
                move |_, _, state: &mut Vikingwm<BackendData>| {
                    let display = state.display.clone();
                    let mut display = display.borrow_mut();
                    match display.dispatch(std::time::Duration::from_millis(0), state)
                    {
                        Ok(_) => Ok(PostAction::Continue),
                        Err(e) =>
                        {
                            error!(state.log, "I/O error on the Wayland display: {}", e);
                            state.running.store(false, Ordering::SeqCst);
                            Err(e)
                        }
                    }
                },
            )
            .expect("Failed to init the wayland event source.");

        // Init a window map, to track the location of our windows
        let mut workspaces = Vec::new();
        for ws in &config.screen.workspaces
        {
            workspaces.push(Workspace::new(ws.to_owned()));
        }

        let output_map = Rc::new( RefCell::new(OutputMap::new(config.screen.clone(),
            display.clone(),
            workspaces.clone(),
            log.unwrap().clone(),
        )?));

        // Init the basic compositor globals
        init_shm_global(&mut (*display).borrow_mut(), vec![], None);

        // Init the shell states
        let shell_states = init_shell::<BackendData>(display.clone(), log);

        init_xdg_output_manager(&mut display.borrow_mut(), None);
        init_xdg_activation_global(
            &mut display.borrow_mut(),
            |state, req, mut ddata| {
                let wmstate = ddata.get::<Vikingwm<BackendData>>().unwrap();
                match req
                {
                    XdgActivationEvent::RequestActivation {
                        token,
                        token_data,
                        surface,
                    } =>
                    {
                        if token_data.timestamp.elapsed().as_secs() < 10
                        {
                            // Just grant the wish
                            wmstate.workspaces[wmstate.current_ws].bring_surface_to_top(&surface);
                        }
                        else
                        {
                            // Discard the request
                            state.lock().unwrap().remove_request(&token);
                        }
                    }
                    XdgActivationEvent::DestroyActivationRequest { .. } =>
                    {}
                }
            },
            None,
        );

        init_xdg_decoration_manager(
            &mut display.borrow_mut(),
            |req, _ddata| match req
            {
                XdgDecorationRequest::NewToplevelDecoration { toplevel } =>
                {
                    use xdg_decoration::v1::server::zxdg_toplevel_decoration_v1::Mode;

                    let res = toplevel.with_pending_state(|state| {
                        state.decoration_mode = Some(Mode::ServerSide);
                    });

                    if res.is_ok()
                    {
                        toplevel.send_configure();
                    }
                }
                XdgDecorationRequest::SetMode { .. } =>
                {}
                XdgDecorationRequest::UnsetMode { .. } =>
                {}
            },
            None,
        );

        let socket_name = if listen_on_socket
        {
            let socket_name = display
                .borrow_mut()
                .add_socket_auto()
                .unwrap()
                .into_string()
                .unwrap();
            info!(log.unwrap(), "Listening on wayland socket"; "name" => socket_name.clone());
            ::std::env::set_var("WAYLAND_DISPLAY", &socket_name);
            Some(socket_name)
        }
        else
        {
            None
        };

        // init data device

        let dnd_icon = Arc::new(Mutex::new(None));

        let dnd_icon2 = dnd_icon.clone();
        init_data_device(
            &mut display.borrow_mut(),
            move |event| match event
            {
                DataDeviceEvent::DnDStarted { icon, .. } =>
                {
                    *dnd_icon2.lock().unwrap() = icon;
                }
                DataDeviceEvent::DnDDropped =>
                {
                    *dnd_icon2.lock().unwrap() = None;
                }
                _ =>
                {}
            },
            default_action_chooser,
            None,
        );

        // init input
        let seat_name = backend_data.seat_name();

        let (mut seat, _) = Seat::new(&mut display.borrow_mut(), seat_name.clone(), None);

        let cursor_status = Arc::new(Mutex::new(CursorImageStatus::Default));

        let cursor_status2 = cursor_status.clone();
        let pointer = seat.add_pointer(move |new_status| {
            // TODO: hide winit system cursor when relevant
            *cursor_status2.lock().unwrap() = new_status
        });

        let keyboard = seat
            .add_keyboard(XkbConfig::default(), 200, 25, |seat, focus| {
                set_data_device_focus(seat, focus.and_then(|s| s.as_ref().client()))
            })
            .expect("Failed to initialize the keyboard");

        #[cfg(feature = "xwayland")]
        let xwayland = {
            let (xwayland, channel) = XWayland::new(handle.clone(), display.clone(), log.clone());
            let ret = handle.insert_source(channel, |event, _, anvil_state| match event
            {
                XWaylandEvent::Ready { connection, client } =>
                {
                    anvil_state.xwayland_ready(connection, client)
                }
                XWaylandEvent::Exited => anvil_state.xwayland_exited(),
            });
            if let Err(e) = ret
            {
                error!(
                    log,
                    "Failed to insert the XWaylandSource into the event loop: {}", e
                );
            }
            xwayland
        };
        let current_ws = 0;
        let keybinds = config.keybinds.get();

        // if let Some(l) = log
        // {
        //     slog::info!(l, "cfg: {}", config);
        //     slog::info!(l, "keybinds:");
        //     for kb in &keybinds
        //     {
        //         slog::info!(l, "{}", kb);
        //     }
        // }
        // TODO: Handle this better
        //let log = log.map(|l| l.clone());
        let log = log.unwrap().clone();
        Ok(Vikingwm {
            config,
            backend_data,
            running: Arc::new(AtomicBool::new(true)),
            display,
            handle,
            workspaces,
            current_ws,
            output_map,
            dnd_icon,
            log,
            socket_name,
            pointer,
            keyboard,
            suppressed_keys: Vec::new(),
            cursor_status,
            pointer_location: (0.0, 0.0).into(),
            seat_name,
            shell_states,
            seat,
            start_time: std::time::Instant::now(),
            keybinds,
            #[cfg(feature = "xwayland")]
            xwayland,
        })
    }

    // pub fn run(cfg: Config, log: &slog::Logger) -> Result<()>
    // {
    //     Ok(())
    // }
}

impl<Backend> Vikingwm<Backend>
{
    pub fn current_ws(&mut self) -> &mut Workspace
    {
        &mut self.workspaces[self.current_ws]
    }

    pub fn move_window_to_workspace(&mut self, ws: usize)
    {
        // Get window from workspace
        // Remove THAT window from workspace
        if let Some(mut w) = self.current_ws().swap_remove()
        {
            // Add THAT window to another workspace
            self.workspaces[ws].insert(&mut w);
        }
    }

    fn get_all_windows(&self) -> Vec<Window>
    {
        let mut window_vec = Vec::new();
        for ws in &self.workspaces
        {
            // TODO: Refactor
            if ws.name != "all"
            {
                // Excluding the 'all'ws and all ws's who are unnamed (aka scratchpads)
                slog::debug!(self.log, "ws.name == {}", ws.name);
                window_vec.extend(ws.windows().to_owned());
            }
        }
        window_vec
    }

    pub fn show_all(&mut self)
    {
        // create hidden workspace
        // reference all windows inside that workspace
        // show that workspace

        let window_vec = self.get_all_windows();
        match self.workspaces.iter().position(|ws| ws.name == "all")
        {
            Some(val) => self.current_ws = val,
            None =>
            {
                //TODO: check current screen
                // If the workspace doesn't exist than create it
                self.workspaces.push(Workspace::new("all".into()));
                self.current_ws = self.workspaces.len() - 1;
            }
        }

        let cws = &mut self.workspaces[self.current_ws];
        cws.clear();
        for mut w in window_vec
        {
            cws.insert(&mut w);
        }
    }

    pub fn run_startup(&self)
    {
        for cmd in &self.config.startup
        {
            debug!(self.log, "running command {}", cmd);
            if let Err(e) = Command::new("sh").arg("-c").arg(cmd).spawn()
            {
                error!(self.log,
                    "Failed to start program";
                    "cmd" => cmd,
                    "err" => format!("{:?}", e)
                );
            }
        }
    }
}

pub trait Backend
{
    fn seat_name(&self) -> String;
}
