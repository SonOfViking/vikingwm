use crate::workspace::workspace;
use smithay::wayland::seat::{Keysym, ModifiersState};

#[derive(Debug, PartialEq, Clone)]
pub enum Direction
{
    Next,
    Prev,
    Up,
    Down,
    Left,
    Right,
    None,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Action
{
    Focus,
    Move,
}

/// Possible results of a keyboard action
#[derive(Debug, PartialEq, Clone)]
pub enum KeyAction
{
    Quit,
    /// Quit the compositor
    Reload,
    /// Reload the compositoR
    VtSwitch(i32),
    /// Trigger a vt-switch
    Run(String),
    /// run a command
    Close,
    /// Only works on currently selected window
    Screen(usize),
    /// Switch the current screen
    Workspace(Action, u8),
    /// Switch/pull in workspaces
    Window(Action, Direction, bool),
    /// Execute action on windows in direction, if bool == true than Action::Move will move it to a different output
    Output(Direction),
    /// Switch focus to output in Direction
    Layout(workspace::Layout),
    ShowAll,
    ScaleUp,
    ScaleDown,
    /// Do nothing more
    None,
}

#[derive(Debug, Clone)]
pub struct Keybind
{
    mods: ModifiersState,
    keys: Keysym,
    action: KeyAction,
}

impl std::fmt::Display for Keybind
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        let mut kb = "".to_owned();
        if self.mods.logo
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "logo";
        }
        if self.mods.alt
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "alt";
        }
        if self.mods.caps_lock
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "caps_lock";
        }
        if self.mods.ctrl
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "ctrl";
        }
        if self.mods.shift
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "shift";
        }
        if self.mods.num_lock
        {
            if kb.len() > 0
            {
                kb += " + ";
            }
            kb += "num_lock";
        }
        kb += " + ";
        kb += &::xkbcommon::xkb::keysym_get_name(self.keys);
        write!(f, "{} -> {:?}", kb, self.action)
    }
}

fn new_empty_modstate() -> ModifiersState
{
    ModifiersState {
        logo: false,
        alt: false,
        caps_lock: false,
        ctrl: false,
        shift: false,
        num_lock: false,
    }
}

impl PartialEq for Keybind
{
    fn eq(&self, other: &Self) -> bool
    {
        self.mods == other.mods && self.keys == other.keys
    }
}

impl Keybind
{
    pub fn from_kb(mods: &ModifiersState, keys: &Keysym) -> Keybind
    {
        Keybind {
            mods: mods.to_owned(),
            keys: keys.to_owned(),
            action: KeyAction::None,
        }
    }

    pub fn action(&self) -> &KeyAction
    {
        &self.action
    }

    pub fn new(keys: Vec<&str>, ka: Option<KeyAction>) -> Keybind
    {
        let mut mods = new_empty_modstate();
        let mut actual_key: Keysym = 0;

        for current in keys
        {
            // dbg!(&::xkbcommon::xkb::keysym_get_name(
            //     ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS)
            // ));
            let key = ::xkbcommon::xkb::keysym_from_name(current, xkbcommon::xkb::KEYSYM_NO_FLAGS);
            if key == xkbcommon::xkb::KEY_NoSymbol
            {
                match current
                {
                    #[cfg(feature = "debug")]
                    "super" => mods.alt = true,
                    #[cfg(not(feature = "debug"))]
                    "super" => mods.logo = true,
                    "ctrl" => mods.ctrl = true,
                    "shift" => mods.shift = true,
                    "num_lock" => mods.num_lock = true,
                    "caps_lock" => mods.caps_lock = true,
                    _ =>
                    {} // xkbcommon::xkb::keysym_from_name(&m, xkbcommon::xkb::KEYSYM_NO_FLAGS)
                }
            }
            else
            {
                actual_key = key
            }
        }
        if let Some(action) = ka
        {
            return Keybind {
                mods,
                keys: actual_key,
                action,
            };
        }
        Keybind {
            mods,
            keys: actual_key,
            action: KeyAction::None,
        }
    }
}
