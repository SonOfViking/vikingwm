# vikingwm

!! THIS IS STILL UNDER DEVELOPMENT !!

Vikingwm is a (WIP) wayland compositor. It is very bare bones, bugs may occur. Try it out at your
own risk.

## Building

If you want to run vikingwm as any other compositor (why though?) use the following:
( requires: 
    pkg-config
    seatd
)
```bash
cargo run --features=release
```

If you want to tryout vikingwm inside of your current compositor use the following:

```bash
cargo run --features=debug
```

An example configuration would be

```toml
# This is run as arg for 'sh -c'
#startup = ["swaybg -m stretch -i /path/to/picture"] # Set background

# In this section everything for the actual output will be set up
[ screen ]

# outputs should be named after the output from 'wlr-randr | grep -E "DP|HDMI"' or 'wlr-randr | rg "DP|HDMI"'
# Order of outputs doesn't matter here
outputs = [
#   name,   resolution,  location,  rotation    refresh,   layout
    ["eDP-1",  "1920x1080",   "0x0",       0,     60,      "Tiling"],
]

# Set up workspaces
workspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]

[ keybinds ]
wm = [
    ["reload", "super + ctrl + r"],
    ["quit", "super + ctrl + q"],
]
spawn = [
    ["firefox", "super + b"],
    ["alacritty", "super + Return"],
]

window = [
    ["next_window", "super + j"], # Focus on the next window
    ["prev_window", "super + k"], # Focus on the previous window
    ["next_output", "super + ."], # Focus on the next output
    ["prev_output", "super + ,"], # Focus on the previous output
    ["move_next_window", "super + shift + J"], # Swap current window with the next window
    ["move_prev_window", "super + shift + K"], # Swap current window with the previous window
    ["move_next_output", "super + shift + >"], # Move current window to the next output
    ["move_prev_output", "super + shift + <"], # Move current window to the previous output
    ["close", "super + q"], # Close current window
    ["set_layout_tiling", "super + t"], # Set layout to the Tiling layout
    ["set_layout_vert_tiling", "super + shift + T"], # Set layout to the VertTiling layout
    ["set_layout_float", "super + shift + F"], # Set layout to the float layout
    ["set_layout_fullscreen", "super + f"], # Set layout to the fullscreen layout
    ["show_all_windows", "super + a"], # Show all windows from every workspace
]

workspace = [
    # these workspaces are indexed based so it doesn't matter what their name is
    ["1", "super + 1"],
    ["2", "super + 2"],
    ["3", "super + 3"],
    ["4", "super + 4"],
    ["5", "super + 5"],
    ["6", "super + 6"],
    ["7", "super + 7"],
    ["8", "super + 8"],
    ["9", "super + 9"],
    ["move_1", "super + shift + 1"], # move window to workspace number
    ["move_2", "super + shift + 2"], # move window to workspace number
    ["move_3", "super + shift + 3"], # move window to workspace number
    ["move_4", "super + shift + 4"], # move window to workspace number
    ["move_5", "super + shift + 5"], # move window to workspace number
    ["move_6", "super + shift + 6"], # move window to workspace number
    ["move_7", "super + shift + 7"], # move window to workspace number
    ["move_8", "super + shift + 8"], # move window to workspace number
    ["move_9", "super + shift + 9"], # move window to workspace number
]
```
